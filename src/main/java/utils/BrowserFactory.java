package utils;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

/**
 * @author gene chernenko returns browser based on the test context or
 *         HtmlUniDriver if no context is given
 */
public class BrowserFactory {
	public static WebDriver getBrowser() throws MalformedURLException {
		String contextBrowser = TestContext.getTestContext().get("browser");
		if (contextBrowser != null) {
			return initBrowser(contextBrowser.toLowerCase());
		}
		return new HtmlUnitDriver();
	}

	/**
	 * @param browser
	 * @return Correspondent WebDriver
	 * @throws MalformedURLException 
	 */	
	private static WebDriver initBrowser(String browser) throws MalformedURLException {
		String br = browser;
		switch (br) {
		case "firefox":
			return new FirefoxDriver();
		case "chrome":
			return new ChromeDriver();
		case "opera":
			return new OperaDriver();
		case "ie":
			return new InternetExplorerDriver();
		case "remote":
			return new RemoteWebDriver(new URL (TestContext.getTestContext().get("selenium_server_url")), DesiredCapabilities.firefox());
		default:
			return new HtmlUnitDriver();
		}
	}
}
