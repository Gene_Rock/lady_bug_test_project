package utils;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

public class MyDriver {
	public static WebDriver Instance;

	public static void Initialize() throws MalformedURLException {
		Instance = BrowserFactory.getBrowser();
		// setting a global implicit wait. So webDriver will wait for the
		// elements for the specified time. 
		Instance.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Instance.manage().window().maximize();
	}
}
