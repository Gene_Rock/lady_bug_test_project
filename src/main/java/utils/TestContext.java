package utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * 
 * read properties file and put them into hashMap
 * @author gene chernenko
 */
public class TestContext {
	public static Map<String, String> testContext = new HashMap<String, String>();

	public static Map<String, String> getTestContext() {
		Properties properties = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream("test_config.properties");
			properties.load(input);

			testContext.put("url", properties.getProperty("url"));
			testContext.put("username", properties.getProperty("username"));
			testContext.put("password", properties.getProperty("password"));
			testContext.put("browser", properties.getProperty("browser"));
			
		} catch (IOException ex) {
			ex.printStackTrace();
			System.out.println(ex.toString());
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return testContext;
	}
}