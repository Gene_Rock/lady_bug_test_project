package com.lady_bug.framework.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage extends AbstractPage {
	public static void GoTo() {
		DRIVER.get(url);
		// force to wait for the element to show up
		new WebDriverWait(DRIVER, 10, 50).until(ExpectedConditions
				.textToBePresentInElement(
						DRIVER.findElement(By.cssSelector(".jumbotron>h1")),
						"LadyBug"));
	}
	
	public static class LoginSubPage {
		public static void loginWithValidCredentials() {
			populateLoginFields(userName, password);
			// clicking on button "LOGIN" without using variable
			DRIVER.findElement(By.name("commit")).click();
		}

		public static void loginWithInvalidCredentials() {
			populateLoginFields("nonexistentUserName", "incorrectPassword");
			DRIVER.findElement(By.name("commit")).click();
			new WebDriverWait(DRIVER, 10, 100).until(ExpectedConditions
					.presenceOfElementLocated(By.id("flash_error")));
		}

		private static void populateLoginFields(String un, String pass) {
			WebElement loginField = DRIVER.findElement(By.id("username"));
			WebElement passwordField = DRIVER.findElement(By.id("password"));
			loginField.sendKeys(un);
			passwordField.sendKeys(pass);
		}

		public static boolean isLoggingErrorGiven() {
			return DRIVER.findElement(By.id("flash_error")).isDisplayed();
		}
	}

	public static void clickLoginButton() {
		WebElement loginButton = DRIVER.findElement(By
				.xpath("html/body/header/div/div/div/div[3]/a"));
		loginButton.click();	
		new WebDriverWait(DRIVER, 5, 50).until(ExpectedConditions.presenceOfElementLocated(By.id("username")));
	}
}