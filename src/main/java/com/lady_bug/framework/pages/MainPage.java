package com.lady_bug.framework.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class MainPage extends AbstractPage {
	private static List<WebElement> topButtons;
	private static List<WebElement> ddmItems;

	private static WebElement logoutButton;
	private static WebElement dropDown;
	private static WebElement viewProfile;
	private static WebElement editProfile;
	private static WebElement ddmLogout;

	public static void clickOnDropDown() {
		getDropDownObject();
		dropDown.click();
	}

	public static void navigateToProfile() {
		clickOnDropDown();
		viewProfile.click();
	}

	public static void navigateToEditProfile() {
		clickOnDropDown();
		editProfile.click();
	}

	private static void getDropDownObject() {
		getNavElements();
		viewProfile = ddmItems.get(1);
		editProfile = ddmItems.get(0);
		ddmLogout = ddmItems.get(2);
	}

	private static void getNavElements() {
		topButtons = DRIVER.findElements(By
				.cssSelector(".btn.btn-success.btn-small"));
		ddmItems = DRIVER.findElements(By.cssSelector(".dropdown-menu>li>a"));
		
		logoutButton = topButtons.get(0);
		dropDown = topButtons.get(1);
	}
}
