package com.lady_bug.framework.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utils.MyDriver;
import utils.LBUtils;
import utils.TestContext;

/**
 * 
 * @author gene chernenko Contains all pages with elements and methods as inner
 *         classes
 */
public abstract class AbstractPage {
	public final static WebDriver DRIVER = MyDriver.Instance;
	public final static String url = TestContext.testContext.get("url");
	public static String userName = TestContext.testContext.get("username");
	public static String password = TestContext.testContext.get("password");
	
	public static class HeaderNavMenu{
		static WebElement logo = DRIVER.findElement(By.cssSelector(".nav>li>img"));
		
		public static boolean isLogoPresent() {
			return logo.isDisplayed();
		}
	}
	
	public static boolean canGetLoginForm() {		
		DRIVER.getCurrentUrl().equals(url + "/login");
		return DRIVER.getCurrentUrl().equals(url + "/login");
	}

	public static boolean isAtLoginPage() {
		return DRIVER.getCurrentUrl()
				.equals("http://lc-ladybug.herokuapp.com/");
	}

	public static boolean isLoginSuccessfull() {
		return DRIVER.findElement(By.id("flash_notice")).getText()
				.equals("You've login!");
	}

	public static void logOut() {
		DRIVER.findElement(By.xpath("html/body/header/div/div/div/div[2]/a"))
				.click();
		new WebDriverWait(DRIVER, 10).until(ExpectedConditions
				.presenceOfElementLocated(By.className("jumbotron")));
		LBUtils.pauseMe(1);
	}

	public static boolean isLogOutSuccessfull() {
		return DRIVER.findElement(By.cssSelector("#flash_notice")).getText()
				.equalsIgnoreCase("You've logged out!");
	}

	public static void closeBrowser() {
		DRIVER.close();
	}

	public static boolean isAtProfilePage(){
		return DRIVER.getCurrentUrl().equals(url + "/users/" + userName);
	}
	
	public static boolean isAtEditProfilePage(){
		return DRIVER.getCurrentUrl().equals(url + "/users/" + userName + "/edit");
	}
}
