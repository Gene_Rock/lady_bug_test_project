package lady_bug;

import java.net.MalformedURLException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import utils.LBUtils;
import utils.MyDriver;
import com.lady_bug.framework.pages.AbstractPage;
import com.lady_bug.framework.pages.LoginPage;
import com.lady_bug.framework.pages.LoginPage.LoginSubPage;

public abstract class BaseTest {
	@BeforeClass
	public static void OpenLoginPage() throws MalformedURLException {
		MyDriver.Initialize();
		LoginPage.GoTo();
		LBUtils.pauseMe(2);
		LoginPage.clickLoginButton();
		LoginSubPage.loginWithValidCredentials();
	}
	
	@AfterClass
	public static void tearDown() {
		AbstractPage.closeBrowser();
	}
}