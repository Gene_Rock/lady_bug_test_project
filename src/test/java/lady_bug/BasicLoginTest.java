package lady_bug;

import java.net.MalformedURLException;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import utils.LBUtils;
import utils.MyDriver;

import com.lady_bug.framework.pages.AbstractPage;
import com.lady_bug.framework.pages.LoginPage;
import com.lady_bug.framework.pages.LoginPage.LoginSubPage;

public class BasicLoginTest extends BaseTest{
	@BeforeClass
	public static void OpenLoginPage() throws MalformedURLException {
		MyDriver.Initialize();
		LoginPage.GoTo();
		LBUtils.pauseMe(2);
		Assert.assertTrue("Application should be able to start.",
				AbstractPage.isAtLoginPage());
	}
	
	@Test
	public void canLoginWithValidCredentials() {
		LoginPage.clickLoginButton();
		Assert.assertTrue("Login sub page should be opened.",
				LoginPage.canGetLoginForm());

		LoginSubPage.loginWithValidCredentials();
		Assert.assertTrue(
				"Login with valid credentials should be successfull.",
				AbstractPage.isLoginSuccessfull());

		AbstractPage.logOut();
		Assert.assertTrue("Logout should be successful.",
				AbstractPage.isLogOutSuccessfull());
	}

	@Test
	public void cannotLoginWithInvalidCredentials() {
		LoginPage.clickLoginButton();
		Assert.assertTrue("Login sub page should be opened.",
				LoginPage.canGetLoginForm());

		LoginSubPage.loginWithInvalidCredentials();
		Assert.assertTrue(
				"Error should be give if user is trying to login with wrong credentials.",
				LoginSubPage.isLoggingErrorGiven());
	}
}