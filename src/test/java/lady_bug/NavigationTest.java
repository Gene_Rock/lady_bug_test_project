package lady_bug;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import com.lady_bug.framework.pages.AbstractPage;
import com.lady_bug.framework.pages.AbstractPage.HeaderNavMenu;
import com.lady_bug.framework.pages.MainPage;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class NavigationTest extends BaseTest{
	// Demo ordered execution. Although well written tests should be executed in
	// random order, sometime some dependencies may be inevitable
	@Test
	public void test01IsLogoPresent() {
		Assert.assertTrue("Logo should be present on the page",
				HeaderNavMenu.isLogoPresent());
	}

	@Test
	public void test02canNavigateProfile() {
		MainPage.navigateToProfile();
		Assert.assertTrue(
				"User should be able to navigate to the profile page.",
				AbstractPage.isAtProfilePage());
	}

	@Test
	public void test03canNavigateToEditProfile() {
		MainPage.navigateToEditProfile();
		Assert.assertTrue("User should be able to navigate to EditProfile page.", AbstractPage.isAtEditProfilePage());
	}

	@AfterClass
	public static void tearDown() {
		AbstractPage.DRIVER.close();
	}
}